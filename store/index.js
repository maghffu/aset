export const state = () => ({
    loggedIn: false,
    loading: false,
    user:{}
  })
  
export const mutations = {
    login (state, user) {
        state.loggedIn = true
        state.user = user
        localStorage.setItem('user', JSON.stringify(user))
    },
    logout (state){
        state.loggedIn =false
        state.user = {}
        localStorage.clear();
    },
    setLoading (state, status){
        state.loading = status
    }

}