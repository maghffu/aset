export default function ({ store, redirect }) {
    // If the user is not authenticated
    if (localStorage.getItem('user')) {
      const user = JSON.parse(localStorage.getItem('user'))
      store.commit('login', user)
    }
    if (!store.state.loggedIn) {
      return redirect('/login')
    }
  }