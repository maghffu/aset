const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const bodyParser = require('body-parser')
const app = express()
const passport = require('passport')
const jwt = require('jsonwebtoken')

/* ROUTING */
const userRoute = require('./routes/users')
const pendapatanRoute = require('./routes/pendapatan')
const reklasifikasiRoute = require('./routes/reklasifikasi')
const pengeluaranRoute = require('./routes/pengeluaran')
const utilRoute = require('./routes/util')

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Give nuxt middleware to express
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())
  app.use(passport.initialize())
  app.use(passport.session()) 
  const strategy = require('./jwt_strategy')
  strategy(passport, '')

  app.use('/api/users', userRoute)
  app.use('/api/pendapatan', pendapatanRoute)
  app.use('/api/reklasifikasi', reklasifikasiRoute)
  app.use('/api/pengeluaran', pengeluaranRoute)
  app.use('/api/util', utilRoute)

  app.use(nuxt.render)
  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
