const { Router } = require('express')
const koneksi = require('../koneksi')
const router = Router()

router.get('/getKib', (req, res) => {
    koneksi.query('SELECT * FROM rekening', (err, rows, field) => {
        if (err) throw err

        return res.json({rows})
    })
})
router.get('/getPendapatan', (req, res) => {
    koneksi.query('SELECT * FROM pendapatan', (err, rows, field) => {
        if (err) throw err

        return res.json({rows})
    })
})
router.get('/sekolah', (req, res) => {
    koneksi.query('SELECT * FROM sekolah', (err, rows, field) => {
        if (err) throw err

        return res.json({rows})
    })
})

module.exports = router;