const { Router } = require('express')
const koneksi = require('../koneksi')
const passport = require('passport')
const router = Router()

router.get('/:jenis', passport.authenticate('jwt', {session : false}), (req, res) => {
    var query ="SELECT dp.*, u.nama_user ,r.nama FROM data_reklas dp JOIN rekening r on r.id = dp.id_rekening JOIN users u on u.id = dp.id_user WHERE id_user = " + req.user.id + ' and reklas =' + req.params.jenis
    if (req.user.hak_akses == 'Admin') {
        query ="SELECT dp.*, u.nama_user ,r.nama FROM data_reklas dp JOIN rekening r on r.id = dp.id_rekening JOIN users u on u.id = dp.id_user WHERE reklas = " + req.params.jenis
    }
    koneksi.query(query, (err, rows, field) => {
        if (err) {koneksi.end(); throw err}
        return res.json({success: true, data: rows})
    })
     
})

router.post('/', passport.authenticate('jwt', {session : false}), (req, res) => {
    const data = req.body.data
    delete data.kib
    delete data.nama_user
    delete data.id
    
    if(data.reklas == 2){
        data.reklas = "Tambah"
    } else {
        data.reklas = "Kurang"
    }
    console.log(data) 
    var k = koneksi.query("INSERT INTO data_reklas SET ?", data , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }
        
        return res.json({success: true, data: rows})
    })
    console.log(k.sql); 
})

router.put('/:id', passport.authenticate('jwt', {session : false}), (req, res) => {
    const data = req.body.data
    delete data.kib
    delete data.nama_user
    delete data.nama

    var k = koneksi.query(`UPDATE data_reklas SET ? WHERE id = ${req.params.id}`, data , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }
        return res.json({success: true, data: rows})
    })
    console.log(k.sql);  
})

router.delete('/hapus/:id',passport.authenticate('jwt', {session : false}), (req, res) => {
    koneksi.query(`DELETE FROM data_reklas WHERE id = ${req.params.id}` , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }

        return res.json({success: true, msg: "Data berhasil dihapus"})
    });
});

module.exports = router;