const { Router } = require('express')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const config = require('../config')
const koneksi = require('../koneksi')

const router = Router()

/* GET users listing. */
router.get('/', function(req, res, next) {
  return res.json({'user' : 'aa'});
});

router.post('/login', (req, res, next) => {
  const username = req.body.username.trim()
  const password = req.body.password
  koneksi.query(` SELECT * FROM users where username = '${username}' ` , (err, rows, field) => {
    if (err) {console.log(err); return res.json({success:false , message : 'Terjadi Kesalahan'}) }
    if (rows.length < 1) {return res.json({success:false , message : 'User tidak ditemukan'}) }
    
    bcrypt.compare(password, rows[0].password, (err, result) => {

      if (result) {
        var user = rows[0]
        delete user.password
        var token = jwt.sign( {id : user.id, hak_akses: user.hak_akses } , config.secret, {
          expiresIn: 604800 //1 week
        })
        user.token = 'JWT ' + token
        return res.json({success:true, user: user })
      }
      return res.json({success:false , message : 'Password tidak Cocok'}) 

    })
  })

  

});


router.get('/setPasword', (req,res) => {
  koneksi.query("SELECT * FROM users WHERE id <> 1", (err, rows) => {
    rows.map(r => {
        const hash = bcrypt.hashSync("disdik123", 10);
        r.password = hash
        koneksi.query("UPDATE users SET ? WHERE id = " + r.id , r)
    })
    return true;
    
  })
})

router.get('/logout/:id', function(req, res, next) {
  const collection = req.db.get('users');
  collection.findOne({_id: req.params.id}, (e,users) => {
    var ua = req.headers['user-agent'];
    if (users.device == ua) {
      users.device = null
      const userdb = req.db.get('users');
      userdb.update({ _id: req.params.id }, {$set: users} , (e,result) => {
        return res.json({success: true})
      })
    }
  })
});


router.get('/profile', passport.authenticate('jwt', {session : false}), (req,res,next) => {
  res.json({user : req.user})
})
router.put('/update/:id', passport.authenticate('jwt', {session : false}), (req,res,next) => {
  var hash = '';
  const user = {
    fullname : req.body.name,
    email : req.body.email,
  }
  if ( typeof req.body.password !== 'undefined' && req.body.password ) {
    hash = bcrypt.hashSync(req.body.password, 10);
    user.password = hash
  }
  
  const collection = req.db.get('users');
  collection.update({ _id: req.params.id }, {$set: user} , (e,result) => {
    return res.json({success: true, message: 'data succesfully updated'})
  })
})

module.exports = router;
