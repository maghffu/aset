const { Router } = require('express')
const koneksi = require('../koneksi')
const passport = require('passport')
const router = Router()

router.get('/', passport.authenticate('jwt', {session : false}), (req, res) => {
    var pencarian = '';
    if( typeof req.body.pencarian != 'undefined' ) pencarian = req.body.pencarian
    var query = `SELECT dp.*, p.nama FROM data_pendapatan dp JOIN pendapatan p on p.id = dp.asal WHERE id_user = ${req.user.id} AND p.nama LIKE '%${pencarian}%'`
    if (req.user.hak_akses == 'Admin') {
        query = `SELECT dp.*, p.nama, u.nama_user FROM data_pendapatan dp JOIN pendapatan p on p.id = dp.asal JOIN users u on u.id = dp.id_user where p.nama LIKE '%${pencarian}%' `
    }
    console.log(query)
    koneksi.query(query, (err, rows, field) => {
        if (err) {koneksi.end(); throw err}
        return res.json({success: true, data: rows, total: rows.length})
    })
     
})

router.post('/', passport.authenticate('jwt', {session : false}), (req, res) => {
    const data = req.body.data
    const o = new Date(data.tanggal)
    const tgl = o.getFullYear() + "-" + (o.getMonth()+1) + "-" + o.getDate()
    data.tanggal = tgl
    console.log(data)
    var k = koneksi.query("INSERT INTO data_pendapatan SET ?", data , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }
        
        
        return res.json({success: true, data: rows})
    })
    console.log(k.sql); 
})

router.put('/:id', passport.authenticate('jwt', {session : false}), (req, res) => {
    const data = req.body.data
    console.log(data)
    var ob = new Date(data.tanggal)
    const tgl = ob.getFullYear() + "-" + (ob.getMonth()+1) + "-" + ob.getDate()
    data.tanggal = tgl
    delete data.nama
    delete data.id
    delete data.id_user
    delete data.nama_user
    var k = koneksi.query(`UPDATE data_pendapatan SET ? WHERE id = ${req.params.id}`, data , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }
        return res.json({success: true, data: rows})
    })
    console.log(k.sql); 
})

router.delete('/:id',passport.authenticate('jwt', {session : false}), (req, res) => {
    koneksi.query(`DELETE FROM data_pendapatan WHERE id = ${req.params.id}` , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }

        return res.json({success: true, msg: "Data berhasil dihapus"})
    })
})

module.exports = router;