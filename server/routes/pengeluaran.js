const { Router } = require('express')
const koneksi = require('../koneksi')
const passport = require('passport')
const router = Router()

router.get('/', passport.authenticate('jwt', {session : false}), (req, res) => {
    var query = "SELECT dp.*, p.nama, r.form,r.id as id_rekening ,r.nama as rekening FROM data_pengeluaran dp JOIN pendapatan p on p.id = dp.id_pendapatan JOIN rekening r on r.id = dp.id_rekening WHERE id_user = " + req.user.id
    if (req.user.hak_akses == 'Admin') {
        query = "SELECT dp.*, p.nama, r.form,r.id as id_rekening ,r.nama as rekening, u.nama_user FROM data_pengeluaran dp JOIN pendapatan p on p.id = dp.id_pendapatan JOIN rekening r on r.id = dp.id_rekening JOIN users u on u.id = dp.id_user"
    }
    koneksi.query(query, (err, rows, field) => {
        if (err) {koneksi.end(); throw err}
        return res.json({success: true, data: rows})
    })
     
})

router.post('/', passport.authenticate('jwt', {session : false}), (req, res) => {
    const data = req.body.data
    delete data.kib
    delete data.id

    data.tanggal_pembelian = new Date(data.tanggal_pembelian).toISOString().split('T')[0]
    data.tanggal_pembukuan = new Date(data.tanggal_pembukuan).toISOString().split('T')[0]
    data.tanggal_sertifikat = new Date(data.tanggal_sertifikat).toISOString().split('T')[0]
    data.tanggal_dokumen = new Date(data.tanggal_dokumen).toISOString().split('T')[0]

    var k = koneksi.query("INSERT INTO data_pengeluaran SET ?", data , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }
        
        
        return res.json({success: true, data: rows})
    })
    console.log(k.sql); 
})

router.put('/:id', passport.authenticate('jwt', {session : false}), (req, res) => {
    const data = req.body.data
    delete data.kib
    delete data.id
    delete data.nama
    delete data.id_user
    delete data.nama_user
    delete data.form
    delete data.rekening

    data.tanggal_pembelian = new Date(data.tanggal_pembelian).toISOString().split('T')[0]
    data.tanggal_pembukuan = new Date(data.tanggal_pembukuan).toISOString().split('T')[0]
    data.tanggal_sertifikat = new Date(data.tanggal_sertifikat).toISOString().split('T')[0]
    data.tanggal_dokumen = new Date(data.tanggal_dokumen).toISOString().split('T')[0]

    
    var k = koneksi.query(`UPDATE data_pengeluaran SET ? WHERE id = ${req.params.id}`, data , (err, rows, field) => {
        if (err) {koneksi.end(); throw err }
        return res.json({success: true, data: rows})
    })
    console.log(k.sql); 
})

router.delete('/hapus/:id',passport.authenticate('jwt', {session : false}), (req, res) => {
    koneksi.query(`DELETE FROM data_pengeluaran WHERE id = ${req.params.id}` , (err, rows, field) => {
        if (err) {koneksi.end(); throw err; }

        return res.json({success: true, msg: "Data berhasil dihapus"});
    });
});

module.exports = router;